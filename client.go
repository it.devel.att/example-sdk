package examplesdk

import "fmt"

// Client ...
type Client struct {
	key string
}

// NewClient ...
func NewClient(key string) *Client {
	return &Client{key: key}
}

// Ping ...
func (c *Client) Ping() {
	fmt.Printf("Hello world, %s\n", c.key)
}
